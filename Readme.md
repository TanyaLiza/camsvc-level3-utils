Level3 Util services
=========================================

This project contains the deployments for the following services:

- __Add Amq Producer v1.0__

	Create a camel route dynamically to push messages to an amq address 
		
- __Send Amq Message v1.0__

	Send message to an amq address through Producer Route already configured to that address
			
- __Remove Amq Producer v1.0__

	Remove camel route dynamically to detach AMQ Producer already configured to send messages to an amq address 
		
- __Add Amq Consumer v1.0__

	Create a camel route dynamically to consume messages from an amq queue
		
- __Remove Amq Consumer v1.0__

	Remove camel route dynamically to detach AMQ Consumer already configured to accept messages from an amq queue 
	



List of Level3 AMQ Util services
################################
-	Add AMQ Producer
-	Send AMQ Message
-	Remove AMQ Producer
-	Add AMQ Consumer
-	Remove AMQ Consumer




The Level3 AMQ set of util services are targetted to address the following Use Cases

QA Use Cases
#############
Right now QA cannot test AMQ services independently. Both AMQ Publisher and AMQ Consumer are tightly dependent on each other.
AMQ Publisher services cannot be tested independently since purge-on-no-consumer=true is enforced by OPs/Build Engineers as messages get purged from queues in the abence of a Subscriber.
AMQ Consumer services cannot be tested independently since there's no means to directly create messages on the queue in the absence of a Producer.

1. Test AMQ publisher independently (subscriber not available)

Steps :
-	Deploy the publisher service to be tested
-	Add consumer using level3 Util Add AMQ Consumer
	POST http://localhost:8080/level3-utils/amq-consumer/add
	{
    		"queue": "order.ods",
    		“filter”: “testing”
	}
-	Send message triggering the publisher service (to be tested)
-	View message
	If flter is set in message, the message will be logged in the server log by level3 Util Add AMQ Consumer
	If filter is not set in message, the message will stay in the queue. The message can be viewed from AMQ web console
-	After testing remove the Consumer to avoid misusage of Send Message service
	DELETE http://localhost:8080/level3-utils/amq-remove/remove?queue="order.ods"


2. Test AMQ subscriber independently (publisher not available)

Steps :
-	Deploy the consumer service to be tested
-	Add producer using level3 Util Add AMQ Producer 
-	POST http://localhost:8080/level3-utils/amq-producer/add
	{
    		"address": "rti.order",
    		"isTopic": "true"
	}
-	Send message using level3 Util Send AMQ Message
	POST http://localhost:8080/level3-utils/amq-producer/send
	Body : message in JSON or XML
-	Test consumer (to be tested)
-	After testing remove the level3 Util AMQ Producer to avoid misusage of Send Message service
	DELETE http://localhost:8080/level3-utils/amq-producer/remove?address="rti.order"



Developer Use Cases
###################
The QA scenarios also apply for Devs.
AMQ Publisher services cannot be tested independently by Developer in DEV since purge-on-no-consumer=true is enforced by OPs and Build Engineers as messages get purged from queues in the absence of a Subscriber.
AMQ Consumer services cannot be developed and tested independently since there's no means to directly create messages on the queue in the absence of a Producer.
Developers quite often have to resort to creating unnecessary ReST endpoints to execute and test consumer service.

1. Test AMQ publisher independently in DEV

Steps :
-	Deploy the developed publisher service to be tested in DEV
-	Add consumer using level3 Util Add AMQ Consumer
	POST http://localhost:8080/level3-utils/amq-consumer/add
	{
    		"queue": "order.ods",
    		“filter”: “testing”
	}
-	Send message triggering the publisher service (to be tested)
-	View message
	If flter is set in message, the message will be logged in the server log by level3 Util Add AMQ Consumer
	If filter is not set in message, the message will stay in the queue. The message can be viewed from AMQ web console
-	After testing remove the Consumer to avoid misusage of Send Message service
	DELETE http://localhost:8080/level3-utils/amq-remove/remove?queue="order.ods"

2. Develop and Test AMQ subscriber independently

Steps :
-	Deploy the developed consumer service to be tested
-	Add producer using level3 Util Add AMQ Producer 
-	POST http://localhost:8080/level3-utils/amq-producer/add
	{
    		"address": "rti.order",
    		"isTopic": "true"
	}
-	Send message using level3 Util Send AMQ Message
	POST http://localhost:8080/level3-utils/amq-producer/send
	Body : message in JSON or XML
-	Test consumer (to be tested)
-	After testing remove the level3 Util AMQ Producer to avoid misusage of Send Message service
	DELETE http://localhost:8080/level3-utils/amq-producer/remove?address="rti.order"


Support Use Cases
#################

Right now if a particular message processed by an AMQ subscriber fails for some reason, that message is logged by the service. In AMQ infrastructure this message would have been propogated through multiple channels and would
have undergone transformation from different upstream services. There is not mechanism right now to directly put this message on the consumed queue to recreate the issue locally. Rather developers will have to take the pain
of figuring out ways to produce such a message in the queue through upstream systems which is quite often unnecessary, time consuming and had dependencies on other systems. A generic service to push messages directly to the queue
would address this developer concern and help the developer to independently run the consumer, recreate the issue locally and thereby troubleshoot.

-Recreate PROD issue on a specific AMQ subscriber independently

Steps :
-	Identify the message causing failure in subscriber service from logs
-	Deploy the production version of consumer service locally
-	Add producer using level3 Util Add AMQ Producer 
-	POST http://localhost:8080/level3-utils/amq-producer/add
	{
    		"address": "rti.order",
    		"isTopic": "true"
	}
-	Send message using level3 Util Send AMQ Message
	POST http://localhost:8080/level3-utils/amq-producer/send
	Body : message in JSON or XML
-	Message will be picked up by consumer. Troubleshoot and debug the issue to get to root cause
-	After testing remove the level3 Util AMQ Producer to avoid misusage of Send Message service
	DELETE http://localhost:8080/level3-utils/amq-producer/remove?address="rti.order"







