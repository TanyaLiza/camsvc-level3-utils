package com.ferguson.service.level3.test;

import java.io.File;
import org.eu.ingwar.tools.arquillian.extension.suite.annotations.ArquillianSuiteDeployment;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.WebArchive;

import com.ferguson.service.testutils.it.IntegrationTestHelper;

@ArquillianSuiteDeployment
public final class ArquillianHelper extends IntegrationTestHelper {

    @Deployment(name = "level3-utils", order = 1, testable = false)
    public static Archive<?> targetDeployment() {
        return ShrinkWrap.createFromZipFile(WebArchive.class, new File("target/level3-utils.war"));
    }

}