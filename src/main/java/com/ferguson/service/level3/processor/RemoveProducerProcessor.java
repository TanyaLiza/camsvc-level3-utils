package com.ferguson.service.level3.processor;

import java.net.HttpURLConnection;

import javax.inject.Named;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.log4j.Logger;

import com.ferguson.service.common.camel.CommonConstants;
import com.ferguson.service.level3.vo.DeleteResponse;

@Named
public class RemoveProducerProcessor implements Processor{

	private static final Logger LOG = Logger.getLogger(RemoveProducerProcessor.class);

		@Override
	public void process(Exchange exchange) throws Exception {
		final DeleteResponse resp = new DeleteResponse();
		if (null == exchange.getIn().getHeader("address")) {
			resp.setIsError(true);
	        resp.setRespCode("remove.amq.producer.invalid.address");
	        resp.setRespMsg(exchange.getIn().getHeader("address") + " is an invalid amq address");
	        resp.setTransId(exchange.getProperty(CommonConstants.FEI_TRANS_ID_PROP, String.class));
	        exchange.setProperty(CommonConstants.FEI_RESP_CODE_PROP, resp.getRespCode());
	        exchange.setProperty(CommonConstants.FEI_LOG_MESSAGE_PROP, resp.getRespMsg());
	        exchange.getIn().setHeader(Exchange.HTTP_RESPONSE_CODE, HttpURLConnection.HTTP_BAD_REQUEST);
	        exchange.getIn().setBody(resp);
	     		}
	}
}
