package com.ferguson.service.level3.processor;

import java.net.HttpURLConnection;

import javax.inject.Named;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.component.bean.validator.BeanValidationException;

import com.ferguson.service.common.camel.CommonConstants;
import com.ferguson.service.common.camel.util.Helpers;
import com.ferguson.service.level3.vo.AddResponse;

@Named
public class BeanValidationExceptionProcessor implements Processor {

    @Override
    public void process(final Exchange exchange) throws Exception {
        
        final AddResponse resp = new AddResponse();
        resp.setIsError(true);
        resp.setRespCode("add.amq.producer.bad.request");
        resp.setRespMsg(Helpers.translateMessageCode(resp.getRespCode()));
        resp.setTransId(exchange.getProperty(CommonConstants.FEI_TRANS_ID_PROP, String.class));
        
        exchange.setProperty(CommonConstants.FEI_RESP_CODE_PROP, resp.getRespCode());
        exchange.setProperty(CommonConstants.FEI_LOG_MESSAGE_PROP, resp.getRespMsg());
        exchange.getIn().setHeader(Exchange.HTTP_RESPONSE_CODE, HttpURLConnection.HTTP_BAD_REQUEST);
        exchange.getIn().setBody(resp);
    }

}
