package com.ferguson.service.level3.processor;
public class BadRequestException extends Exception {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public BadRequestException(final String errorMessage) {
        super(errorMessage);
    }
}
