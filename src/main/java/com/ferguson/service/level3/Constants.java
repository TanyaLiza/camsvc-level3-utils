package com.ferguson.service.level3;

public final class Constants {
    public static final String REQUEST_FORMAT_ERROR = "request.format.error";
    public static final String ERROR_PROCESSING_REQUEST = "error.processing.request";    
    public static final String BAD_REQUEST = "bad.request";
    public static final String ROUTE_ID="route.id";
    public static final String PROCESS_ERROR = "process.error";
    public static final String MANDATORY_VALIDATION_ERROR = "mandatory.validation.error";    
    public static final String INTERNAL_ERROR = "internal.error";
    
    public static final String EXCHANGE_BODY = "${body}";
    public static final String STACK_TRACE = "${exception.stacktrace}";
	public static final String CONSUMER_REG_SUCCESS = "consumer.reg.success";
	public static final String CONSUMER_REMOVE_SUCCESS = "consumer.remove.success";
	public static final String ACTIVEMQ = "activemq";

    private Constants() {
    }
}