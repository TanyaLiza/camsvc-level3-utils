package com.ferguson.service.level3.cdi;

import javax.enterprise.inject.Produces;
import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import org.apache.camel.PropertyInject;
import org.apache.camel.component.properties.PropertiesComponent;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.camel.component.ActiveMQComponent;
import org.apache.activemq.camel.component.ActiveMQConfiguration;
import org.apache.activemq.pool.PooledConnectionFactory;


@Named
@ApplicationScoped
public class CDIProducers {

    public static final String UNKNOWN = "unknown";

    @PropertyInject("amq.url")
    private String amqUrl;

    @PropertyInject(value = "amq.pool.max", defaultValue = "10")
    private Integer amqPoolSize;

    @PropertyInject(value = "amq.user", defaultValue = UNKNOWN)
    private String amqUser;

    @PropertyInject(value = "amq.pwd", defaultValue = UNKNOWN)
    private String amqPwd;
    
    @PropertyInject("amq.blockiffull")
    private Boolean amqBlockIfFull;

    private final ActiveMQComponent amq = new ActiveMQComponent();


    @PostConstruct
    public void init() {
        final ActiveMQConnectionFactory amqc = new ActiveMQConnectionFactory(amqUser, amqPwd, amqUrl);
        final PooledConnectionFactory pooledConnectionFactory = new PooledConnectionFactory(amqc);
        pooledConnectionFactory.setMaxConnections(amqPoolSize);
        pooledConnectionFactory.setBlockIfSessionPoolIsFull(amqBlockIfFull);
        final ActiveMQConfiguration jmsConfig = new ActiveMQConfiguration();
//        jmsConfig.setDurableSubscriptionName("level3-utils");
        jmsConfig.setConnectionFactory(pooledConnectionFactory);
        amq.setConfiguration(jmsConfig);
    }

    @Named("activemq")
    @Produces
    public ActiveMQComponent initAMQComponent() {
        return amq;
    }

    @Produces
    @Named("properties")
    PropertiesComponent propertiesComponent() {
        final PropertiesComponent pc = new PropertiesComponent();
        // load properties file form the file system.
        pc.setLocation("file:${jboss.server.config.dir}/FEI-Props/level3-utils.properties");
        return pc;
    }
}
